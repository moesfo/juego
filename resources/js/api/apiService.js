import Vue from "vue";
import Axios from "axios";
import VueAxios from "vue-axios";
Vue.use(VueAxios, Axios);

export default {
    validateUser(id_user, pass) {
        let body = { user: id_user, password: pass };
        return Axios.post("./login", body);
    },
    getQuestions(id_module) {
        return Axios.get("./questions/" + id_module);
    },
    postAnswer(payload) {
        return Axios.get(
            "./answer?question=" +
                payload.id_question +
                "&correct=" +
                payload.is_correct +
                "&time=" +
                payload.spent_time +
                "&user=" +
                payload.user
        );
    },
    getRanking() {
        return Axios.get("./ranking");
    },
    checkProgress() {
        return Axios.get("./check_progress");
    }
};
