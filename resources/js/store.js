import Vue from "vue";
import Vuex from "vuex";
import ApiService from "./api/apiService";
Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        //example: []
        user: "",
        password: "",
        questions: null,
        errorMessage: ""
    },
    mutations: {
        SET_CREDENTIALS(state, user, password) {
            state.user = user;
            state.password = password;
        },
        SET_QUESTIONS(state, questions) {
            state.questions = questions;
        },
        SET_ERROR(state, error) {
            state.errorMessage = error;
        }
    },
    actions: {
        setCredentials(context, user, password) {
            context.commit("SET_CREDENTIALS", user, password);
        },
        setCredentials(context, error) {
            context.commit("SET_ERROR", error);
        },
        getQuestions(context, id_module) {
            ApiService.getQuestions(id_module)
                .then(response => {
                    context.commit("SET_QUESTIONS", response.data.data);
                })
                .catch(function(error) {
                    console.log(
                        error.response.status
                    ); /* eslint-disable-line no-console */
                    context.commit("SET_ERROR", error.response.status);
                });
        }
    }
});
