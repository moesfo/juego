import Vue from "vue";
import VueRouter from "vue-router";
import LoginComponent from "./components/LoginComponent.vue";
import HomeComponent from "./components/HomeComponent.vue";
import StartComponent from "./components/StartComponent.vue";

Vue.use(VueRouter);

export default new VueRouter({
    routes: [
        {
            path: "/",
            redirect: "/login"
        },
        {
            path: "/login",
            name: "login",
            component: LoginComponent
        },
        {
            path: "/home",
            name: "home",
            component: HomeComponent,
            props: true
        },
        {
            path: "/start",
            name: "start",
            component: StartComponent,
            props: true
        }
    ]
});
