<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Acciones & Valores | Desafío SISTEMA DE GESTIÓn</title>
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <!-- Styles -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
        <link href="https://fonts.googleapis.com/css?family=Satisfy&display=swap" rel="stylesheet">
        <style>
            html, body{
                height: 100% !important;
            }

            canvas {
            display: block;
            vertical-align: bottom;
            }

            /* ---- particles.js container ---- */

            #particles-js {
            position: absolute;
            width: 100%;
            height: 100%;
            background-color: #92161b;
            background-image: url("");
            background-repeat: no-repeat;
            background-size: cover;
            background-position: 50% 50%;
            }
        </style>
    </head>
    <body>
        <div id="particles-js" style="position: absolute"></div>
        <div id="app"></div>
        <script src="{{ asset('js/app.js') }}"></script>
        <script src="{{ asset('js/particles.js') }}"></script>
        <script>
            particlesJS.load('particles-js', 'js/particles.json', function () {
                //console.log('callback - particles.js config loaded');
            });
        </script>
    </body>
</html>
