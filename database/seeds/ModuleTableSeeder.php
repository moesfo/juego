<?php

use Illuminate\Database\Seeder;

class ModuleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('module')->delete();

        \DB::table('module')->insert([
            0 =>
            [
                'id' => 1,
                'description' => 'SARO',
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            1 =>
            [
                'id' => 2,
                'description' => 'SARLAFT',
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            2 =>
            [
                'id' => 3,
                'description' => 'SISTEMA DE GESTIÓN DE SEGURIDAD Y SALUD EN EL TRABAJO (SG SST)',
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            3 =>
            [
                'id' => 4,
                'description' => 'SISTEMA DE ATENCIÓN AL CONSUMIDOR FINANCIERO - SAC',
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            4 =>
            [
                'id' => 5,
                'description' => 'SISTEMA DE SEGURIDAD DE LA INFORMACIÓN Y CIBERSEGURIDAD - SICbS',
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            5 =>
            [
                'id' => 6,
                'description' => 'SARM-SARL-SARiC (Riesgos Financieros)',
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
        ]);
    }
}
