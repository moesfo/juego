<?php

use Illuminate\Database\Seeder;

class GroupTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('group')->delete();

        \DB::table('group')->insert([
            0 =>
            [
                'id' => 1,
                'description' => 'MM',
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            1 =>
            [
                'id' => 2,
                'description' => 'Bursatil',
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
        ]);
    }
}
