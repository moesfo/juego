<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('user')->delete();

        \DB::table('user')->insert([
            0 =>
            [
                'id' => 1,
                'full_name' => 'Monica Espinel',
                'password' => '123456',
                'email' => 'mespinelf@gmail.com',
                'id_group' => 1,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ]
        ]);
    }
}
