<?php

use Illuminate\Database\Seeder;

class QuestionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('question')->delete();

        \DB::table('question')->insert([
            0 =>
            [
                'id' => 1,
                'description' => '¿Cuál de las siguientes opciones corresponde a un evento de Riesgo Operativo?',
                'id_module' => 1,
                'points' => 1000,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            1 =>
            [
                'id' => 2,
                'description' => 'Algunos de los elementos del SARO son :',
                'id_module' => 1,
                'points' => 1000,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            2 =>
            [
                'id' => 3,
                'description' => '¿Cuál es la importancia de gestionar el Riesgo Operativo?',
                'id_module' => 1,
                'points' => 1000,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            3 =>
            [
                'id' => 4,
                'description' => 'Si Daniel toma como base para la identificación de riesgos los eventos de riesgo operativo ocurridos, está aplicando:',
                'id_module' => 1,
                'points' => 1000,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            4 =>
            [
                'id' => 5,
                'description' => 'Pablo identificó una situación de fraude en su puesto de trabajo ¿qué debe hacer Pablo al respecto?',
                'id_module' => 1,
                'points' => 1000,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            5 =>
            [
                'id' => 6,
                'description' => 'Por error de digitación María realizó un abono a la cuenta de un cliente por mayor valor, el dinero se debe recobrar generando reprocesos e inconformidades al cliente. Este evento se puede clasificar como:',
                'id_module' => 1,
                'points' => 1000,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            6 =>
            [
                'id' => 7,
                'description' => 'Qué se debe tener en cuenta a la hora de reportar un Evento de Riesgo Operativo',
                'id_module' => 1,
                'points' => 1000,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            7 =>
            [
                'id' => 8,
                'description' => '¿Quién es el responsable de realizar el reporte de eventos de Riesgo Operativo?',
                'id_module' => 1,
                'points' => 1000,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            8 =>
            [
                'id' => 9,
                'description' => '¿Cuáles son los factores generadores de Riesgo Operativo?',
                'id_module' => 1,
                'points' => 1000,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            9 =>
            [
                'id' => 10,
                'description' => '¿Cuáles son los riesgos asociados al Riesgo Operativo?',
                'id_module' => 1,
                'points' => 1000,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            10 =>
            [
                'id' => 11,
                'description' => 'Seleccione la opción que describe las siglas SARLAFT',
                'id_module' => 2,
                'points' => 1000,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            11 =>
            [
                'id' => 12,
                'description' => '¿Que significa Lavado de activos?',
                'id_module' => 2,
                'points' => 1000,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            12 =>
            [
                'id' => 13,
                'description' => '¿Que significa Financiación del terrorismo?',
                'id_module' => 2,
                'points' => 1000,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            13 =>
            [
                'id' => 14,
                'description' => 'Indique las etapas de SARLAFT',
                'id_module' => 2,
                'points' => 1000,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            14 =>
            [
                'id' => 15,
                'description' => '¿Cuáles de las siguientes respuestas pueden ser consideradas señales de alerta frente a SARLAFT?',
                'id_module' => 2,
                'points' => 1000,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            15 =>
            [
                'id' => 16,
                'description' => 'Seleccione la opción que describe las siglas GAFI',
                'id_module' => 2,
                'points' => 1000,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            16 =>
            [
                'id' => 17,
                'description' => 'Una de los objetivos del GAFI es',
                'id_module' => 2,
                'points' => 1000,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            17 =>
            [
                'id' => 18,
                'description' => '¿Cuántas recomendaciones emitió el GAFI para combatir el lavado de activos, el financiamiento del terrorismo y las amenazas relacionadas con el sistema financiero internacional?',
                'id_module' => 2,
                'points' => 1000,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            18 =>
            [
                'id' => 19,
                'description' => 'Una tipología de riesgo SARLAFT es',
                'id_module' => 2,
                'points' => 1000,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            19 =>
            [
                'id' => 20,
                'description' => 'Mencione alguno de los delitos fuentes de LA/FT',
                'id_module' => 2,
                'points' => 1000,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            20 =>
            [
                'id' => 21,
                'description' => '¿Que es el Sistema de Gestión de Seguridad y Salud en el Trabajo ( SG SST)?',
                'id_module' => 3,
                'points' => 1000,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            21 =>
            [
                'id' => 22,
                'description' => '¿Que es un incidente de trabajo?',
                'id_module' => 3,
                'points' => 1000,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            22 =>
            [
                'id' => 23,
                'description' => '¿Cuales son las funciones del COPASST?',
                'id_module' => 3,
                'points' => 1000,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            23 =>
            [
                'id' => 24,
                'description' => '¿Cuáles son los riesgos Biomecánicos?',
                'id_module' => 3,
                'points' => 1000,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            24 =>
            [
                'id' => 25,
                'description' => '¿Cuales son los compromisos de Acciones y Valores frente al Sistema de Gestión de Seguridad y Salud en el Trabajo?',
                'id_module' => 3,
                'points' => 1000,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            25 =>
            [
                'id' => 26,
                'description' => '¿A quien debe reportar los incidentes/accidentes de trabajo?',
                'id_module' => 3,
                'points' => 1000,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            26 =>
            [
                'id' => 27,
                'description' => '"Adoptar una posición incorrecta en los puestos de trabajo" Hace referencia a:',
                'id_module' => 3,
                'points' => 1000,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            27 =>
            [
                'id' => 28,
                'description' => '¿Cuál es el periodo de permanencia del Comité de Convivencia Laboral y el COPASST?',
                'id_module' => 3,
                'points' => 1000,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            28 =>
            [
                'id' => 29,
                'description' => 'Participar en las actividades de formación en Seguridad y Salud en el Trabajo, es rol/responsabilidad de:',
                'id_module' => 3,
                'points' => 1000,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            29 =>
            [
                'id' => 30,
                'description' => '¿Que documento define el marco normativo para tener en cuenta la prevencion de los Accidentes de trabajo y Enfermedades Laborales?',
                'id_module' => 3,
                'points' => 1000,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            30 =>
            [
                'id' => 31,
                'description' => '¿En que consiste el sistema de atención al consumidor financiero?',
                'id_module' => 4,
                'points' => 1000,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            31 =>
            [
                'id' => 32,
                'description' => '¿Cuál de los siguientes son los derechos y deberes del consumidor financiero?',
                'id_module' => 4,
                'points' => 1000,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            32 =>
            [
                'id' => 33,
                'description' => '¿Cuales son los deberes de Acciones y Valores como entidad vigilada?',
                'id_module' => 4,
                'points' => 1000,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            33 =>
            [
                'id' => 34,
                'description' => '¿Cuál es el objetivo de la educación financiera?',
                'id_module' => 4,
                'points' => 1000,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            34 =>
            [
                'id' => 35,
                'description' => '¿Cuál es el papel del defensor de consumidor financiero en Acciones y Valores?',
                'id_module' => 4,
                'points' => 1000,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            35 =>
            [
                'id' => 36,
                'description' => '¿Cuáles son las etapas del SAC?',
                'id_module' => 4,
                'points' => 1000,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            36 =>
            [
                'id' => 37,
                'description' => 'El requerimiento de tipo queja, a que hace referencia?',
                'id_module' => 4,
                'points' => 1000,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            37 =>
            [
                'id' => 38,
                'description' => '¿Cuáles son los canales de atención que tiene Acciones & Valores S.A. para la atención de los Consumidores Financieros?',
                'id_module' => 4,
                'points' => 1000,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            38 =>
            [
                'id' => 39,
                'description' => 'Mencione tres entes de vigilancia',
                'id_module' => 4,
                'points' => 1000,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            39 =>
            [
                'id' => 40,
                'description' => '¿El requerimiento de tipo reclamo, a que hace referencia?',
                'id_module' => 4,
                'points' => 1000,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            40 =>
            [
                'id' => 41,
                'description' => '¿El objetivo de Seguridad de la Información y Ciberseguridad es?',
                'id_module' => 5,
                'points' => 1000,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            41 =>
            [
                'id' => 42,
                'description' => 'La política general de seguridad de la información y ciberseguridad garantiza',
                'id_module' => 5,
                'points' => 1000,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            42 =>
            [
                'id' => 43,
                'description' => '¿Cuáles son los principios de Seguridad de la Información y Ciberseguridad?',
                'id_module' => 5,
                'points' => 1000,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            43 =>
            [
                'id' => 44,
                'description' => 'Cual es la responsabilidad de los funcionarios de Acciones & Valores frente al sistema de seguridad de la información y ciberseguridad?',
                'id_module' => 5,
                'points' => 1000,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            44 =>
            [
                'id' => 45,
                'description' => 'Si un funcionario de Acciones & Valores abre un archivo adjunto de un correo que desconoce, el incidente de seguridad se genera a causa de:',
                'id_module' => 5,
                'points' => 1000,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            45 =>
            [
                'id' => 46,
                'description' => 'Los incidentes de seguridad se pueden generar de fuentes externas o internas. Algunos ejemplos que describen estas fuentes son:',
                'id_module' => 5,
                'points' => 1000,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            46 =>
            [
                'id' => 47,
                'description' => '¿Cuales son lo tipos de incidentes de seguridad de la información?',
                'id_module' => 5,
                'points' => 1000,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            47 =>
            [
                'id' => 48,
                'description' => 'Los incidentes de seguridad de la información en muchas ocasiones pueden ser deliberados o accidentales. Algunos ejemplos de estos pueden ser:',
                'id_module' => 5,
                'points' => 1000,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            48 =>
            [
                'id' => 49,
                'description' => 'Los incidentes de seguridad de la información en muchas ocasiones pueden ser deliberados o accidentales. Algunos ejemplos de estos pueden ser:',
                'id_module' => 5,
                'points' => 1000,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            49 =>
            [
                'id' => 50,
                'description' => '¿Qué se debe tener en cuenta para hacer que una contraseña tenga un buen nivel de seguridad?:',
                'id_module' => 5,
                'points' => 1000,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            50 =>
            [
                'id' => 51,
                'description' => 'El VaR es un modelo que permite cuantificar',
                'id_module' => 6,
                'points' => 1000,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            51 =>
            [
                'id' => 52,
                'description' => 'Cuál de los siguientes indicadores es necesario para calcular el LAR:',
                'id_module' => 6,
                'points' => 1000,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            52 =>
            [
                'id' => 53,
                'description' => 'El modelo CAMEL no tiene en cuenta este factor:',
                'id_module' => 6,
                'points' => 1000,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            53 =>
            [
                'id' => 54,
                'description' => 'Cuál de las siguientes afirmaciones es cierta respecto a la volatilidad:',
                'id_module' => 6,
                'points' => 1000,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            54 =>
            [
                'id' => 55,
                'description' => 'Que beneficios trae las certificaciones otorgadas por el AMV',
                'id_module' => 6,
                'points' => 1000,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            55 =>
            [
                'id' => 56,
                'description' => 'Cuál de los siguientes aspectos no se tienen en cuenta en la clasificación de activos por perfil (conservador, moderado y arriesgado):',
                'id_module' => 6,
                'points' => 1000,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            56 =>
            [
                'id' => 57,
                'description' => 'Cuáles condiciones deben cumplirse para la realización de un repo: 1. El cliente debe contar con un contrato de Repos vigente. 2. Se deben depositar las garantías correspondientes a la operación en la CRCC. 3. Si el subyacente del repo es un activo restringido por Acciones y Valores la operación debe contar con la autorización del área de riesgos. 4.Estar autorizada por el área de riesgos.',
                'id_module' => 6,
                'points' => 1000,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            57 =>
            [
                'id' => 58,
                'description' => 'Las siguientes órdenes podrían ser exoneradas del registro en el Libro Electrónico de Ordenes (LEO) con previa autorización del área de riesgos:',
                'id_module' => 6,
                'points' => 1000,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            58 =>
            [
                'id' => 59,
                'description' => 'Cuantificar el nivel mínimo de activos líquidos en moneda nacional y extranjera a cuál de las siguientes etapas de un sistema de administración de riesgos corresponde:',
                'id_module' => 6,
                'points' => 1000,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            59 =>
            [
                'id' => 60,
                'description' => 'Asumiendo un escenario en el cual el Banco de la República incremente sus tasas de interés y como consecuencia se generen desvaloraciones de diferentes títulos en el mercado, así como aumento en las tasas de fondeo (todo lo demás constante), cuál(es) de los siguientes riesgos se estaría materializando:',
                'id_module' => 6,
                'points' => 1000,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            60 =>
            [
                'id' => 61,
                'description' => 'Una caída de la Relación de Solvencia puede presentarse por:',
                'id_module' => 6,
                'points' => 1000,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
        ]);
    }
}
