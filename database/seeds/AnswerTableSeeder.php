<?php

use Illuminate\Database\Seeder;

class AnswerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('answer')->delete();

        \DB::table('answer')->insert([
            0 =>
            [
                'id' => 1,
                'description' => 'Ruptura de vidrios.',
                'is_correct' => 0,
                'id_question' => 1,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            1 =>
            [
                'id' => 2,
                'description' => 'Fallas en el software interno.',
                'is_correct' => 0,
                'id_question' => 1,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            2 =>
            [
                'id' => 3,
                'description' => 'Sobrantes en caja.',
                'is_corrrect' => 0,
                'id_question' => 1,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            3 =>
            [
                'id' => 4,
                'description' => 'Todas las anteriores.',
                'is_correct' => 1,
                'id_question' => 1,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            4 =>
            [
                'id' => 5,
                'description' => 'Políticas, oficinas de representación, plataforma tecnológica y recurso humano.',
                'is_correct' => 0,
                'id_question' => 2,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            5 =>
            [
                'id' => 6,
                'description' => 'Políticas, oficinas de representación, plataforma tecnológica y recurso humano.',
                'is_correct' => 0,
                'id_question' => 2,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            6 =>
            [
                'id' => 7,
                'description' => 'Actividades de apoyo, divulgación de información y organos de control.',
                'is_correct' => 0,
                'id_question' => 2,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            7 =>
            [
                'id' => 8,
                'description' => 'Plataforma tecnológica, capacitación, registro de eventos de riesgo operativo, estructura organizacional y documentación.',
                'is_correct' => 1,
                'id_question' => 2,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            8 =>
            [
                'id' => 9,
                'description' => 'Gestionar el mejoramiento de los procesos, mejorar la calidad de los productos y servicios, apoyo para toma de decisiones.',
                'is_correct' => 1,
                'id_question' => 3,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            9 =>
            [
                'id' => 10,
                'description' => 'Cumplir con los informes y requerimiento de las áreas internas de la compañía.',
                'is_correct' => 0,
                'id_question' => 3,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            10 =>
            [
                'id' => 11,
                'description' => 'Investigar situaciones irregulares y definir las sanciones para las mismas.',
                'is_correct' => 0,
                'id_question' => 3,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            11 =>
            [
                'id' => 12,
                'description' => 'Fomentar las buenas prácticas en terminos de liquidez de la compañía',
                'is_correct' => 0,
                'id_question' => 3,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            12 =>
            [
                'id' => 13,
                'description' => 'La disminución de impacto.',
                'is_correct' => 0,
                'id_question' => 4,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            13 =>
            [
                'id' => 14,
                'description' => 'El enfoque cuantitativo.',
                'is_correct' => 1,
                'id_question' => 4,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            14 =>
            [
                'id' => 15,
                'description' => 'El monitoreo de los riesgos operativos.',
                'is_correct' => 0,
                'id_question' => 4,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            15 =>
            [
                'id' => 16,
                'description' => 'El enfoque cualitativo.',
                'is_correct' => 0,
                'id_question' => 4,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            16 =>
            [
                'id' => 17,
                'description' => 'Comentar con sus compañeros el caso.',
                'is_correct' => 0,
                'id_question' => 5,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            17 =>
            [
                'id' => 18,
                'description' => 'Investigar la situación a profundidad para obtener más detalles.',
                'is_correct' => 0,
                'id_question' => 5,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            18 =>
            [
                'id' => 19,
                'description' => 'No comentar ni informar nada al respecto.',
                'is_correct' => 0,
                'id_question' => 5,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            19 =>
            [
                'id' => 20,
                'description' => 'Informar a su jefe inmediato y realizar el reporte del evento de riesgo operativo a través del formulario asignado o correo electrónico.',
                'is_correct' => 0,
                'id_question' => 5,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            20 =>
            [
                'id' => 21,
                'description' => 'Relaciones laborales.',
                'is_correct' => 0,
                'id_question' => 6,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            21 =>
            [
                'id' => 22,
                'description' => 'Fraude interno.',
                'is_correct' => 0,
                'id_question' => 6,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            22 =>
            [
                'id' => 23,
                'description' => 'Clientes.',
                'is_correct' => 0,
                'id_question' => 6,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            23 =>
            [
                'id' => 24,
                'description' => 'Ejecución y administración de proyectos.',
                'is_correct' => 1,
                'id_question' => 6,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            24 =>
            [
                'id' => 25,
                'description' => 'La disponibilidad del formulario.',
                'is_correct' => 0,
                'id_question' => 7,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            25 =>
            [
                'id' => 26,
                'description' => 'Qué lo generó, el suceso y el impacto que tuvo o podría tener.',
                'is_correct' => 1,
                'id_question' => 7,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            26 =>
            [
                'id' => 27,
                'description' => 'El suceso y quién lo generó.',
                'is_correct' => 0,
                'id_question' => 7,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            27 =>
            [
                'id' => 28,
                'description' => 'El funcionario que lo generó y su jefe inmediato.',
                'is_correct' => 0,
                'id_question' => 7,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            28 =>
            [
                'id' => 29,
                'description' => 'El gerente de riesgos.',
                'is_correct' => 0,
                'id_question' => 8,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            29 =>
            [
                'id' => 30,
                'description' => 'Todos los empleados de la compañía.',
                'is_correct' => 1,
                'id_question' => 8,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            30 =>
            [
                'id' => 31,
                'description' => 'El jefe de cada área.',
                'is_correct' => 0,
                'id_question' => 8,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            31 =>
            [
                'id' => 32,
                'description' => 'El gerente general.',
                'is_correct' => 0,
                'id_question' => 8,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            32 =>
            [
                'id' => 33,
                'description' => 'Prevención, detección y respuesta.',
                'is_correct' => 0,
                'id_question' => 9,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            33 =>
            [
                'id' => 34,
                'description' => 'Confidencialidad, disponibilidad e integridad.',
                'is_correct' => 0,
                'id_question' => 9,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            34 =>
            [
                'id' => 35,
                'description' => 'Recurso humano, procesos, tecnología, infraestructura y acontecimientos externos.',
                'is_correct' => 1,
                'id_question' => 9,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            35 =>
            [
                'id' => 36,
                'description' => 'Autocontrol, autorregulación, autogestión.',
                'is_correct' => 0,
                'id_question' => 9,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            36 =>
            [
                'id' => 37,
                'description' => 'Riesgo Emergente y Riesgo de Contagio.',
                'is_correct' => 0,
                'id_question' => 10,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            37 =>
            [
                'id' => 38,
                'description' => 'Riesgo Legal y Riesgo Reputacional.',
                'is_correct' => 1,
                'id_question' => 10,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            38 =>
            [
                'id' => 39,
                'description' => 'Riesgo de Liquidez y Riesgo de Crédito.',
                'is_correct' => 0,
                'id_question' => 10,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            39 =>
            [
                'id' => 40,
                'description' => 'Riesgo de Mercado y Riesgo público.',
                'is_correct' => 0,
                'id_question' => 10,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            40 =>
            [
                'id' => 41,
                'description' => 'Sistema de Administración del Riesgo de Lavado de Activos y Financiación del Terrorismo.',
                'is_correct' => 1,
                'id_question' => 11,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            41 =>
            [
                'id' => 42,
                'description' => 'Sistema de Administración del Fraude y Financiación del Terrorismo.',
                'is_correct' => 0,
                'id_question' => 11,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            42 =>
            [
                'id' => 43,
                'description' => 'Sistema para el Desarrollo de la Financiación del Terrorismo y Lavado de Activos.',
                'is_correct' => 0,
                'id_question' => 11,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            43 =>
            [
                'id' => 44,
                'description' => 'Sistema Administrativo y Control del Riesgo de Lavado de Activos y Financiación del Terrorismo.',
                'is_correct' => 0,
                'id_question' => 11,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            44 =>
            [
                'id' => 45,
                'description' => 'Lavado de activos: Dar apariencia de legalidad a bienes o dinero adquiridos ilícitamente.',
                'is_correct' => 1,
                'id_question' => 12,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            45 =>
            [
                'id' => 46,
                'description' => 'Lavado de activos: Es el proceso de hacer que dinero "limpio" parezca "sucio".',
                'is_correct' => 0,
                'id_question' => 12,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            46 =>
            [
                'id' => 47,
                'description' => 'Lavado de activos: Es el apoyo financiero, de cualquier forma, al terrorismo o a aquéllos que lo fomentan.',
                'is_correct' => 0,
                'id_question' => 12,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            47 =>
            [
                'id' => 48,
                'description' => 'Ninguna de las anteriores.',
                'is_correct' => 0,
                'id_question' => 12,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            48 =>
            [
                'id' => 49,
                'description' => 'Apoyo financiero, de cualquier forma, al terrorismo o a aquéllos que lo fomentan.',
                'is_correct' => 1,
                'id_question' => 13,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            49 =>
            [
                'id' => 50,
                'description' => 'Apoyar cualquier actividad financiera.',
                'is_correct' => 0,
                'id_question' => 13,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            50 =>
            [
                'id' => 51,
                'description' => 'Es el proceso de hacer que el dinero "sucio" parezca "limpio".',
                'is_correct' => 0,
                'id_question' => 13,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            51 =>
            [
                'id' => 52,
                'description' => 'Ninguna de las anteriores.',
                'is_correct' => 0,
                'id_question' => 13,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            52 =>
            [
                'id' => 53,
                'description' => 'Identicación y evaluación',
                'is_correct' => 0,
                'id_question' => 14,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            53 =>
            [
                'id' => 54,
                'description' => 'Análisis, detección y evaluación.',
                'is_correct' => 0,
                'id_question' => 14,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            54 =>
            [
                'id' => 55,
                'description' => 'Identificación, evaluación, control y monitoreo.',
                'is_correct' => 1,
                'id_question' => 14,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            55 =>
            [
                'id' => 56,
                'description' => 'Reporte y control del riesgo.',
                'is_correct' => 0,
                'id_question' => 14,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            56 =>
            [
                'id' => 57,
                'description' => 'Desconoce la información se las transacciones, nerviosa, entrega el dinero a otras personas.',
                'is_correct' => 0,
                'id_question' => 15,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            57 =>
            [
                'id' => 58,
                'description' => 'Trae la información escrita como: nombre de la contraparte, país de donde le envían y el monto aproximado.',
                'is_correct' => 0,
                'id_question' => 15,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            58 =>
            [
                'id' => 59,
                'description' => 'Recibe múltiples operaciones en un periodo de tiempo corto, de diferentes personas.',
                'is_correct' => 0,
                'id_question' => 15,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            59 =>
            [
                'id' => 60,
                'description' => 'Todas las anteriores.',
                'is_correct' => 1,
                'id_question' => 15,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            60 =>
            [
                'id' => 61,
                'description' => 'Grupo de Atención Financiera Internacional',
                'is_correct' => 0,
                'id_question' => 16,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            61 =>
            [
                'id' => 62,
                'description' => 'Grupo Andino de Financiamiento Internacional',
                'is_correct' => 0,
                'id_question' => 16,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            62 =>
            [
                'id' => 63,
                'description' => 'Grupo de Acción Financiera Internacional',
                'is_correct' => 1,
                'id_question' => 16,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            63 =>
            [
                'id' => 64,
                'description' => 'Grupo de Acción Contra Financiación ilegal',
                'is_correct' => 0,
                'id_question' => 16,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            64 =>
            [
                'id' => 65,
                'description' => 'Fijar normas internacionales en pro del lavado de activos, el financiamiento del terrorismo y amenazas relacionadas con el sistema financiero internacional.',
                'is_correct' => 0,
                'id_question' => 17,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            65 =>
            [
                'id' => 66,
                'description' => 'Fijar estándares y promover la implementación de medidas legales, regulatorias y operativas para combatir el lavado de activos, el financiamiento del terrorismo y amenazas relacionadas con el sistema financiero internacional.',
                'is_correct' => 1,
                'id_question' => 17,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            66 =>
            [
                'id' => 67,
                'description' => 'Establecer la parametrización para controlar el lavado de activos, el financiamiento del terrorismo y amenazas relacionadas con el sistema financiero internacional en América Latina',
                'is_correct' => 0,
                'id_question' => 17,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            67 =>
            [
                'id' => 68,
                'description' => 'Unificación de las naciones a nivel mundial, con el fin de fijar un nivel de riesgo aceptable acerca del lavado de activos, el financiamiento del terrorismo y amenazas relacionadas con el sistema financiero internacional.',
                'is_correct' => 0,
                'id_question' => 17,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            68 =>
            [
                'id' => 69,
                'description' => '80',
                'is_correct' => 0,
                'id_question' => 18,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            69 =>
            [
                'id' => 70,
                'description' => '20',
                'is_correct' => 0,
                'id_question' => 18,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            70 =>
            [
                'id' => 71,
                'description' => '10',
                'is_correct' => 0,
                'id_question' => 18,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            71 =>
            [
                'id' => 72,
                'description' => '40',
                'is_correct' => 1,
                'id_question' => 18,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            72 =>
            [
                'id' => 73,
                'description' => 'Clientes con información financiera desactualizada.',
                'is_correct' => 0,
                'id_question' => 19,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            73 =>
            [
                'id' => 74,
                'description' => 'Representantes legales extranjeros.',
                'is_correct' => 0,
                'id_question' => 19,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            74 =>
            [
                'id' => 75,
                'description' => 'Importación y exportación de empresas de papel.',
                'is_correct' => 1,
                'id_question' => 19,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            75 =>
            [
                'id' => 76,
                'description' => 'Empresas con servicios intangibles.',
                'is_correct' => 0,
                'id_question' => 19,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            76 =>
            [
                'id' => 77,
                'description' => 'Delitos contra el sistema financiero, Extorsión, Concierto para delinquir',
                'is_correct' => 1,
                'id_question' => 20,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            77 =>
            [
                'id' => 78,
                'description' => 'Concierto para delinquir, Homicidio, Abuso de Confianza.',
                'is_correct' => 0,
                'id_question' => 20,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            78 =>
            [
                'id' => 79,
                'description' => 'Hurto, Enriquecimiento ilícito, Rebelión.',
                'is_correct' => 0,
                'id_question' => 20,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
            79 =>
            [
                'id' => 80,
                'description' => 'Acceso carnal violento, Delitos contra el sistema financiero, Homicidio.',
                'is_correct' => 0,
                'id_question' => 20,
                'status' => 1,
                'created_at' => '2019-10-06 15:00:00',
                'updated_at' => '2019-10-06 15:00:00',
            ],
        ]);
    }
}
