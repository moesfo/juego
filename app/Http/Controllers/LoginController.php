<?php

namespace App\Http\Controllers;

use Session;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function verify(Request $request)
    {
        $validatedData = $request->validate([
            'user' => 'required|integer',
            'password' => 'required|integer'
        ]);

        if(!$validatedData){
            return $this->responseJson([], 203, 'Campos invalidos');
        }

        $user = User::find($request->user);

        if ($user) {
            if($user->password == $request->password){
                Session::put('user', $request->user);
                return $this->responseJson([], 200);
            }else{
                return $this->responseJson([], 400, 'Datos incorrectos');
            }            
        } else {
            return $this->responseJson([], 400, 'Datos incorrectos');
        }
    }

    public function logout(Request $request)
    {
        $request->session()->flush();
        
        return $this->responseJson([], 203, 'Logout');
    }
}
