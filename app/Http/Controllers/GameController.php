<?php

namespace App\Http\Controllers;

use DB;
use Session;
use App\Models\Answer;
use App\Models\Module;
use App\Models\Progress;
use App\Models\Question;
use App\Models\Config;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use phpDocumentor\Reflection\Types\Collection;

class GameController extends Controller
{
    public function checkProgress(Request $request)
    {
        $user = Session::get('user');
        $config = Config::where('key', 'question')->first();
        $value = $config ? $config->value : 10;

        if ($user) {

            $progress = Progress::where('id_user', $user)->first();
            
            if(!$progress){
                $return = ['points' => 0, 'module' => 1];
                return $this->responseJson($return, 200);
            }
            
            $points = Progress::select(
                DB::raw("(SUM(p.is_correct * q.points)) as points")
            )
                ->where('p.id_user', $user)
                ->where('p.is_correct', 1)
                ->from('progress as p')
                ->join('question as q', 'q.id', 'p.id_question')
                ->first();
            
            $module = Progress::select(
                'q.id_module'
            )
                ->where('p.id_user', $user)
                ->from('progress as p')
                ->join('question as q', 'q.id', 'p.id_question')
                ->orderBy('q.id_module', 'desc')
                ->get();

            $count = $module->count();
            $total = ceil($count / $value);
            if($count % $value == 0){
                $total++;
            }
            
            $check = ['points' => $points->points, 'module' => $total];

            if (count($check) == 0) {
                return $this->responseJson([], 203);
            }

            return $this->responseJson($check, 200);
        }

        return $this->responseJson([], 401, 'Token no encontrado');
    }

    public function allModules(Request $request)
    {
        return Module::select('id', 'description')->get();
    }

    public function getQuestions(Request $request, $module)
    {
        $user = Session::get('user');
        $config = Config::where('key', 'question')->first();
        $value = $config ? $config->value : 30;
        $total = $value;

        if ($user) {

            $check = Progress::select(
                'q.id as question'
            )
                ->where('p.id_user', $user)
                ->from('progress as p')
                ->join('question as q', 'q.id', 'p.id_question')
                ->orderBy('question', 'desc')
                ->get();

            // if (count($check) > 0) {
            //     $count = $check->where('module', $module)->count();
            //     $total = $value - $count;
            // }

            $question = Question::select(
                'q.id',
                'q.description',
                'q.points',
                'q.id_module'
            )
                ->from('question as q')
                ->whereNotIn('q.id', $check)
                ->where('q.status', 1)
                //->leftJoin('progress as p', 'q.id', 'p.id_question')
                ->inRandomOrder()
                ->limit($total)
                ->get();

            foreach ($question as $q) {
                $q['answers'] = Answer::select(
                    'id',
                    'description',
                    'is_correct'
                )
                    ->where('id_question', $q->id)
                    ->inRandomOrder()
                    ->get();
            }
            return $this->responseJson($question, 200);
        }

        return $this->responseJson([], 401, 'Token no encontrado');
    }

    public function saveProgress(Request $request)
    {
        $user = Session::get('user');

        if ($user) {

            $validatedData = $request->validate([
                'question' => 'required|integer',
                'correct' => 'required',
                'time' => 'required|integer',
            ]);

            if (!$validatedData) {
                return $this->responseJson([], 203, 'Campos invalidos');
            }

            $progress = new Progress;
            $progress->id_question = $request->question;
            $progress->id_user = $user;
            $progress->is_correct = $request->correct;
            $progress->spent_time = $request->time;
            $progress->save();

            return $this->responseJson([], 200, 'guardado con éxito');
        }

        return $this->responseJson([], 401, 'Token no encontrado');
    }

    public function calculateRanking(Request $request)
    {
        $user = Session::get('user');

        if ($user) {
            $ranking = Progress::select(
                'u.full_name as name',
                DB::raw("(SUM((p.is_correct = 1) * q.points)) as puntaje"),
                DB::raw("SUM(p.spent_time) as time")
            )
                ->from('progress as p')
                ->join('question as q', 'q.id', 'p.id_question')
                ->join('user as u', 'p.id_user', 'u.id')
                ->groupBy('u.full_name')
                ->orderBy('puntaje', 'desc')
                ->orderBy('time', 'asc')
                ->limit(10)
                ->get();

            if (!$ranking) {
                return $this->responseJson([], 203, 'No existen datos');
            }
            return $this->responseJson($ranking, 200);
        }

        return $this->responseJson([], 401, 'Token no encontrado');
    }
}
