<?php


namespace App\Traits;

use Illuminate\Http\Response;

trait ApiResponser
{
    /**
     * Se utilizara para formatear las validaciones
     *
     * @param Validator $validator
     *
     * @return array or boolean;
     *
     */

    protected function formatValidator($validator)
    {
        if ($validator->fails()) {
            return [
                "data" => ["errors" => $validator->getMessageBag()->toArray()],
                "code" => Response::HTTP_BAD_REQUEST,
                "result_code" => 'field_validation_error',
                "message" => trans('auth.errors_validate')
            ];
        } else {
            return false;
        }
    }

    /**
     * Se utilizara para mostrar les mensajes de exito y de error del mismo api-gateway
     *
     * @param array $data
     * @param integer code
     * @param string $status
     * @param $message
     *
     * @return JsonResponse;
     *
     */

    public function responseJson($data, $code = Response::HTTP_OK, $message = '', $resultCode = '')
    {
        $dataReturn = $data;
        if (isset($data['errors'])) {
            $dataReturn = [
                "errors" => [
                    "fields_error" => $data['errors']
                ]

            ];
        }
        return response()->json(['status' => self::listResponseHttp($code), 'code' => $code,
            "result_code" => $resultCode, 'locale' => app('translator')->getLocale(),
            "message" => $message,
            'data' => $dataReturn], $code);
    }

    /**
     * Se utilizara para mostrar el mensaje dependiendo del codigo HTPP
     *
     * @param integer $code
     *
     * @return string
     * 
     */

    protected static function listResponseHttp($code)
    {
        $codeHttp = array(
            //1×× Informational
            100 => ['Continue', 'WARNING'],
            101 => ['Switching Protocols', 'WARNING'],
            102 => ['Processing', 'WARNING'],
            //2×× Success
            200 => ['OK', 'SUCCESS'],
            201 => ['Created', 'SUCCESS'],
            202 => ['Accepted', 'SUCCESS'],
            203 => ['Non-authoritative Information', 'WARNING'],
            204 => ['No Content', 'WARNING'],
            205 => ['Reset Content', 'WARNING'],
            206 => ['Partial Content', 'WARNING'],
            207 => ['Multi-Status', 'WARNING'],
            208 => ['Already Reported', 'WARNING'],
            226 => ['IM Used', 'WARNING'],
            //3×× Redirection
            300 => ['Multiple Choices', 'WARNING'],
            301 => ['Moved Permanently', 'WARNING'],
            302 => ['Found', 'WARNING'],
            303 => ['See Other', 'WARNING'],
            304 => ['Not Modified', 'WARNING'],
            305 => ['Use Proxy', 'WARNING'],
            306 => ['Switch Proxy', 'WARNING'],
            307 => ['Temporary Redirect', 'WARNING'],
            308 => ['Permanent Redirect', 'WARNING'],
            //4×× Client Error
            400 => ['Bad Request', 'ERROR'],
            401 => ['Unauthorized', 'WARNING'],
            402 => ['Payment Required', 'ERROR'],
            403 => ['Forbidden', 'ERROR'],
            404 => ['Not Found', 'ERROR'],
            405 => ['Method Not Allowed', 'ERROR'],
            406 => ['Not Acceptable', 'ERROR'],
            407 => ['Proxy Authentication Required', 'ERROR'],
            408 => ['Request Timeout', 'ERROR'],
            409 => ['Conflict', 'ERROR'],
            410 => ['Gone', 'ERROR'],
            411 => ['Length Required', 'ERROR'],
            412 => ['Precondition Failed', 'ERROR'],
            413 => ['Request Entity Too Large', 'ERROR'],
            414 => ['Request-URI Too Long', 'ERROR'],
            415 => ['Unsupported Media Type', 'ERROR'],
            416 => ['Requested Range Not Satisfiable', 'ERROR'],
            417 => ['Expectation Failed', 'ERROR'],
            418 => ['I\'m a teapot', 'ERROR'],
            419 => ['Authentication Timeout', 'ERROR'],
            420 => ['Enhance Your Calm', 'ERROR'],
            420 => ['Method Failure', 'ERROR'],
            422 => ['Unprocessable Entity', 'WARNING'],
            423 => ['Locked', 'ERROR'],
            424 => ['Failed Dependency', 'ERROR'],
            424 => ['Method Failure', 'ERROR'],
            425 => ['Unordered Collection', 'ERROR'],
            426 => ['Upgrade Required', 'ERROR'],
            428 => ['Precondition Required', 'ERROR'],
            429 => ['Too Many Requests', 'ERROR'],
            431 => ['Request Header Fields Too Large', 'ERROR'],
            444 => ['No Response', 'ERROR'],
            449 => ['Retry With', 'ERROR'],
            450 => ['Blocked by Windows Parental Controls', 'ERROR'],
            451 => ['Redirect', 'ERROR'],
            451 => ['Unavailable For Legal Reasons', 'ERROR'],
            494 => ['Request Header Too Large', 'ERROR'],
            495 => ['Cert Error', 'ERROR'],
            496 => ['No Cert', 'ERROR'],
            497 => ['HTTP to HTTPS', 'ERROR'],
            499 => ['Client Closed Request', 'ERROR'],
            //5×× Server Error
            500 => ['Internal Server Error', 'ERROR'],
            501 => ['Not Implemented', 'ERROR'],
            502 => ['Bad Gateway', 'ERROR'],
            503 => ['Service Unavailable', 'ERROR'],
            504 => ['Gateway Timeout', 'ERROR'],
            505 => ['HTTP Version Not Supported', 'ERROR'],
            506 => ['Variant Also Negotiates', 'ERROR'],
            507 => ['Insufficient Storage', 'ERROR'],
            508 => ['Loop Detected', 'ERROR'],
            509 => ['Bandwidth Limit Exceeded', 'ERROR'],
            510 => ['Not Extended', 'ERROR'],
            511 => ['Network Authentication Required', 'ERROR'],
            598 => ['Network read timeout error', 'ERROR'],
            599 => ['Network connect timeout error', 'ERROR'],
        );
        return (array_key_exists($code, $codeHttp)) ? $codeHttp[$code][1] : 'The http response does not exist';
    }

}
